import sys,os,shutil

field_maps=[]

possible_folders=['6.0_Spoke','7.0_Medium_Beta','8.0_High_Beta']

def append_fieldmap_names(line):
    '''
    check if command line contains
    the name of a field map
    '''
    cmd=line.split()[0]
    if cmd.lower()=='field_map':
        map_name=line.split()[-1]
        map_type=line.split()[1]
        map_dict=dict(name=map_name,type=map_type)
        if map_dict not in field_maps:
            field_maps.append(map_dict)

def copy_files():
    '''
    copy all files to current folder
    '''
    extensions=[
            "edz",
            "edx",
            "edy",
            "bdx",
            "bdy",
            "bdz",
            ]
    for _map in field_maps:
        map_name=_map["name"]
        map_type=_map["type"]
        found=False
        for folder in possible_folders:
            full_path=os.path.join(folder,'Field_Maps')
            filepath=os.path.join(full_path,map_name+'.'+extensions[0])
            if os.path.isfile(filepath):
                print "Found",map_name,"in",full_path
                shutil.copy(filepath,'.')
                if map_type=="7700": # 3D map..
                    for e in extensions[1:]:
                        filepath=os.path.join(full_path,map_name+'.'+e)
                        shutil.copy(filepath,'.')
                found=True
                break
        if not found:
            raise ValueError("Could not find "+name)

def main():

    lattice_file='lattice.dat'
    if len(sys.argv)==2:
        if os.path.isfile(sys.argv[-1]):
            lattice_file=sys.argv[-1]
    
    for l in file(lattice_file,'r'):
        if len(l.strip())>0:
            append_fieldmap_names(l)

    copy_files()

if __name__=="__main__":
    main()
