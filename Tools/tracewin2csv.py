#
#
# Parse Tracewin lattice data files and produce slot and element information for each section
# Tracewin data files are located in GIT: gitlab01.esss.lu.se:ess-bp/ess-lattice.git
#
# Using "next_PBI_hinko" branch DAT files that have markers and DIAG_xxxx for PBI
#
# TODO: Handling of BPM indexes for DTL (use DTL cell index?)
#       Handling of first BEND at the end of HEBT (which slot?)
#       Handling of complete A2T (define slots)

import sys
import re
import os
from math import fabs, ceil, sin, cos, pi

class Geo():
    def __init__(self, label):
        self.label = label
        # x, y, z axis
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        # accumulated beam path length
        self.s = 0.0
        # accumulated lattice length, along Z axis
        self.l = 0.0

class Section():
    def __init__(self, name):
        self.fullName = name
        self.subsections = []
        # section length in path of beam
        self.l = 0.0
        # MCS geometry at the start, middle and end of section
        self.mcsStart = Geo('MCS START')
        self.mcsMid = Geo('MCS MIDDLE')
        self.mcsEnd = Geo('MCS END')
        # TCS geometry at the start, middle and end of section
        self.tcsStart = Geo('TCS START')
        self.tcsMid = Geo('TCS MIDDLE')
        self.tcsEnd = Geo('TCS END')

    def getSubsection(self, name):
        for ss in self.subsections:
            if ss.name == name:
                return ss
        return None

class Subsection():
    def __init__(self, section, cell, slot):
        self.section = section
        self.cell = cell
        self.slot = slot
        self.name = cell
        if len(slot):
            self.name += '_' + slot
        self.fullName = self.section + '_' + self.name
        self.elements = []
        # subsection length in path of beam
        self.l = 0.0
        # MCS geometry at the start, middle and end of subsection
        self.mcsStart = Geo('MCS START')
        self.mcsMid = Geo('MCS MIDDLE')
        self.mcsEnd = Geo('MCS END')
        # TCS geometry at the start, middle and end of subsection
        self.tcsStart = Geo('TCS START')
        self.tcsMid = Geo('TCS MIDDLE')
        self.tcsEnd = Geo('TCS END')

class Element():
    def __init__(self, line):
        self.line = line
        self.tokens = re.split('\s+', line)
        self.section = 'XXX'
        self.cell = 'YYY'
        self.slot = 'ZZZ'
        self.index = 0
        self.name = 'AAA'
        self.fullName = 'DDD'
        self.tag = "CCC"
        # element length in path of beam
        self.l = 0.0
        # MCS geometry at the start, middle and end of element
        self.mcsStart = Geo('MCS START')
        self.mcsMid = Geo('MCS MIDDLE')
        self.mcsEnd = Geo('MCS END')
        # TCS geometry at the start, middle and end of element
        self.tcsStart = Geo('TCS START')
        self.tcsMid = Geo('TCS MIDDLE')
        self.tcsEnd = Geo('TCS END')
        # rotation of element
        self.angle = 0.0
        # rotation radius of the rotation (for BEND elements only)
        self.radius = 0.0
        # pipe aperture
        self.r = 0.0
        self.label = ''
        self.disc = ''
        self.show = False
        self.valid = False
        self.commentTokens = []

        self.findComments()
        self.parseLine()

    def __repr__(self):
        return "%s length '%f' mm, radius '%f' mm, center MCS x,y,z '%f,%f,%f' mm, center TCS x,y,z '%f,%f,%f' mm" % \
            (self.fullName, self.l, self.r,
             self.mcsMid.x, self.mcsMid.y, self.mcsMid.z,
             self.tcsMid.x, self.tcsMid.y, self.tcsMid.z)

    def findComments(self):
        semicolon = 0
        for tok in self.tokens:
            semicolon += 1
            if tok == ';':
                self.commentTokens = self.tokens[semicolon:]

    def parseLine(self):
        # need at least three words
        if len(self.tokens) < 2:
            return
        # do we have a label?
        if self.tokens[1] == ':':
            self.label = self.tokens[0]
            self.tokens = self.tokens[2:]
        # uppercase the tracewin tag
        self.tag = self.tokens[0].upper()
        # include the element in output by default, can be false later on
        self.show = True
        # set element short name and length
        if self.tag == 'MARKER':
            self.name = self.tokens[1]
            self.show = False
        elif self.tag == 'DRIFT':
            self.l = float(self.tokens[1])
            self.r = float(self.tokens[2])
            self.name = 'DRF'
            # ignore drifts shorter than 1 mm
            if self.l < 1.0:
                self.show = False
        elif self.tag == 'SOLENOID':
            self.l = float(self.tokens[1])
            self.r = float(self.tokens[3])
            self.name = 'SOL'
        elif self.tag == 'RFQ_CELL':
            self.l = float(self.tokens[5])
            self.name = 'RFC'
        elif self.tag == 'QUAD':
            self.l = float(self.tokens[1])
            self.r = float(self.tokens[3])
            self.name = 'QUA'
        elif self.tag == 'DTL_CEL':
            self.l = float(self.tokens[1])
            self.r = float(self.tokens[9])
            self.name = 'DTC'
        elif self.tag == 'FIELD_MAP':
            self.l = float(self.tokens[2])
            self.r = float(self.tokens[4])
            self.name = 'CAV'
        elif self.tag == 'SUPERPOSE_MAP':
            self.l = float(self.tokens[1])
            self.name = 'SPM'
            self.show = False
        elif self.tag == 'BEND':
            self.angle = fabs(float(self.tokens[1]))
            self.radius = float(self.tokens[2])
            self.l = ceil(self.angle * pi / 180.0 * self.radius)
            self.r = float(self.tokens[4])
            self.name = 'BND'
        elif self.tag == 'EDGE':
            self.name = 'EDG'
            self.r = float(self.tokens[6])
        elif self.tag == 'THIN_STEERING':
            self.name = 'COR'
            self.r = float(self.tokens[3])
        elif self.tag == 'DIAG_EMIT':
            # actual device name is first word after the comment start
            if len(self.commentTokens) == 0:
                self.show = False
            else:
                self.name = self.commentTokens[0]
                self.disc = 'PBI'
            self.disc = 'PBI'
        elif self.tag == 'DIAG_SIZE':
            # actual device name is first word after the comment start
            if len(self.commentTokens) == 0:
                self.show = False
            else:
                self.name = self.commentTokens[0]
                self.disc = 'PBI'
            self.disc = 'PBI'
        elif self.tag == 'DIAG_CURRENT':
            # actual device name is first word after the comment start
            if len(self.commentTokens) == 0:
                self.show = False
            else:
                self.name = self.commentTokens[0]
                self.disc = 'PBI'
            self.disc = 'PBI'
        elif self.tag == 'DIAG_POSITION':
            # actual device name is always BPM
            self.name = 'BPM'
            self.disc = 'PBI'
        elif self.tag == ';#DIAG_FC':
            self.name = 'FC'
            self.disc = 'PBI'
        elif self.tag == ';#DIAG_LOSS':
            self.name = 'BLM'
            self.disc = 'PBI'
        elif self.tag == 'APERTURE':
            # actual device name is first word after the comment start
            if len(self.commentTokens) == 0:
                self.show = False
            else:
                self.name = self.commentTokens[0]
                self.disc = 'PBI'
        else:
            #print(' IGN :', self.tag)
            self.show = False
            # this element can be deleted..
            return False

        self.valid = True
        #print(' OK  :', self.name, self.l, self.commentTokens)
        # element successfully parsed
        return True

    def isMarker(self):
        return (self.tag == 'MARKER')

    def outputElement(self, filehandle, position):
        if not self.show:
            return

        mcs = None
        tcs = None
        if position == 'START':
            mcs = self.mcsStart
            tcs = self.tcsStart
        elif position == 'MIDDLE':
            mcs = self.mcsMid
            tcs = self.tcsMid
        elif position == 'END':
            mcs = self.mcsEnd
            tcs = self.tcsEnd
        else:
            print('Invalid position %s specified' % position)
            return False

        d = '%7s, %7s, %7s, %7s, %7s, %7s, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f' % \
                         ('"%s"' % self.section,
                          '"%s"' % self.cell,
                          '"%s"' % self.slot,
                          '"%s"' % self.name,
                          '"%03d"' % self.index,
                          '"%s"' % self.disc,
                          self.l,
                          self.r,
                          mcs.s,
                          mcs.x,
                          mcs.y,
                          mcs.z,
                          tcs.x,
                          tcs.y,
                          tcs.z)
        #print(d)
        filehandle.write(d+'\n')
        return True

class Lattice():
    def __init__(self, args):
        self.kind = args.kind
        self.folder = args.folder
        self.datFile = args.datfile

        self.elements = []
        self.slotIndexes = dict()
        self.elementIndexes = dict()
        self.cell = ''
        self.slot = ''
        self.elementIndexMult = 1
        self.section = ''
        self.subsection = ''
        self.sections = []

    def run(self):
        print('Lattice is of kind %s' % self.kind)

        ret = self.readFile()
        if not ret:
            return False

        self.partitionLattice()
        self.calculateMCS()
        self.calculateTCS()

        self.outputElements('START')
        self.outputElements('MIDDLE')
        self.outputElements('END')

        self.outputSubsections('START')
        self.outputSubsections('MIDDLE')
        self.outputSubsections('END')

        self.outputSections()

        return True


    def getSection(self, name):
        for s in self.sections:
            if s.fullName == name:
                return s
        return None

    def readFile(self):
        angle = 0.0
        radius = 0.0
        file = self.datFile
        if not os.path.isfile(file):
            print('File %s does not exist!' % file)
            return False

        f = open(file, 'r')
        for line in f:
            line = line.strip(' \t\n\r')
            ##print('Line:', line)
            # skip empty lines
            if not len(line):
                continue

            el = Element(line)
            if not el.valid:
                continue

            if el.name == 'BND' and angle == 0.0:
                # this is first BEND so remember radius and angle
                angle = fabs(float(el.tokens[1]))
                radius = float(el.tokens[2])
            elif el.name == 'BND' and angle != 0.0:
                # reset the angle and radius for after second BEND
                angle = 0.0
                radius = 0.0
            else:
                # use saved values for other elements
                el.angle = angle
                el.radius = radius

            self.elements.append(el)
        f.close()
        print('Parsed TraceWin DAT file %s (created %d elements)..' % (file, len(self.elements)))
        return True

    def partitionLattice(self):
        for el in self.elements:
            if el.isMarker():
                self.handleMarker(el)
            else:
                # element indexes
                if el.name in self.elementIndexes:
                    self.elementIndexes[el.name] += 1
                else:
                    self.elementIndexes[el.name] = 1

                # basic params
                el.section = self.section
                el.cell = self.cell
                el.slot = self.slot
                el.index = self.elementIndexes[el.name] * self.elementIndexMult
                if len(el.slot):
                    el.fullName = el.section + '_' + el.cell + '_' + el.slot + '_' + el.name + '_' + '%03d' % el.index
                else:
                    el.fullName = el.section + '_' + el.cell + '_' + el.name + '_' + '%03d' % el.index
                # add to the subsection
                section = self.getSection(self.section)
                if section!=None:
                    subsection = section.getSubsection(self.subsection)
                    subsection.elements.append(el)

        # get rid of the empty subsections
        for s in self.sections:
            for ss in s.subsections:
                if len(ss.elements) == 0:
                    s.subsections.remove(ss)

    def calculateMCS(self):
        # calculate MCS for elements
        # previous element
        prevEl = None
        degToRad = pi / 180.0
        for el in self.elements:
            if not el.isMarker():
                # dy is increase up, in Y direction
                # dz is increase along the lattice, not beam path
                if el.name == 'BND':
                    dz = el.radius * sin(el.angle * degToRad)
                    dy = 2 * el.radius * sin(el.angle / 2.0 * degToRad) * sin(el.angle / 2.0 * degToRad)
                else:
                    dz = el.l * cos(el.angle * degToRad)
                    dy = el.l * sin(el.angle * degToRad)

                if el.name == 'SPM' or not prevEl:
                    # this is for first element in the lattice and
                    # for superpose elements which set the lattice shift offset
                    # value for next field map

                    # start coordinates are already at 0.0
                    # middle coordinates
                    el.mcsMid.x = 0.0
                    el.mcsMid.y = 0.0
                    el.mcsMid.z = el.l / 2.0
                    el.mcsMid.s = el.l / 2.0
                    el.mcsMid.l = el.l / 2.0
                    # end coordinates
                    el.mcsEnd.x = 0.0
                    el.mcsEnd.y = 0.0
                    el.mcsEnd.z = el.l
                    el.mcsEnd.s = el.l
                    el.mcsEnd.l = el.l
                else:
                    # this is for the rest of the elements

                    # start coordinates
                    el.mcsStart.x = prevEl.mcsEnd.x
                    el.mcsStart.y = prevEl.mcsEnd.y
                    el.mcsStart.z = prevEl.mcsEnd.z
                    el.mcsStart.s = prevEl.mcsEnd.s
                    el.mcsStart.l = prevEl.mcsEnd.l
                    # middle coordinates
                    el.mcsMid.x = el.mcsStart.x
                    el.mcsMid.y = el.mcsStart.y + dy / 2.0
                    el.mcsMid.z = el.mcsStart.z + dz / 2.0
                    el.mcsMid.s = el.mcsStart.s + el.l / 2.0
                    el.mcsMid.l = el.mcsStart.l + dz / 2.0
                    # end coordinates
                    el.mcsEnd.x = el.mcsStart.x
                    el.mcsEnd.y = el.mcsStart.y + dy
                    el.mcsEnd.z = el.mcsStart.z + dz
                    el.mcsEnd.s = el.mcsStart.s + el.l
                    el.mcsEnd.l = el.mcsStart.l + el.l

                    # set element aperture if needed
                    if el.r == 0.0:
                        el.r = prevEl.r

                # remember this element for next iteration
                prevEl = el

        # calculate MCS for sections and subsections
        for s in self.sections:
            # first calculate subsections positions
            for ss in s.subsections:
                # start is at first element of subsection
                ss.mcsStart.x = ss.elements[0].mcsStart.x
                ss.mcsStart.y = ss.elements[0].mcsStart.y
                ss.mcsStart.z = ss.elements[0].mcsStart.z
                ss.mcsStart.s = ss.elements[0].mcsStart.s
                ss.mcsStart.l = ss.elements[0].mcsStart.l
                # end is at the last element of subsection
                ss.mcsEnd.x = ss.elements[-1].mcsEnd.x
                ss.mcsEnd.y = ss.elements[-1].mcsEnd.y
                ss.mcsEnd.z = ss.elements[-1].mcsEnd.z
                ss.mcsEnd.s = ss.elements[-1].mcsEnd.s
                ss.mcsEnd.l = ss.elements[-1].mcsEnd.l
                # middle is the middle point between end and start
                ss.mcsMid.x = (ss.mcsEnd.x - ss.mcsStart.x) / 2.0 + ss.mcsStart.x
                ss.mcsMid.y = (ss.mcsEnd.y - ss.mcsStart.y) / 2.0 + ss.mcsStart.y
                ss.mcsMid.z = (ss.mcsEnd.z - ss.mcsStart.z) / 2.0 + ss.mcsStart.z
                ss.mcsMid.s = (ss.mcsEnd.s - ss.mcsStart.s) / 2.0 + ss.mcsStart.s
                ss.mcsMid.l = (ss.mcsEnd.l - ss.mcsStart.l) / 2.0
                # length of the subsection
                ss.l = ss.mcsEnd.l - ss.mcsStart.l

            # second calculate the section position using subsection position calculated above
            # start is at first aubsection of section
            s.mcsStart.x = s.subsections[0].mcsStart.x
            s.mcsStart.y = s.subsections[0].mcsStart.y
            s.mcsStart.z = s.subsections[0].mcsStart.z
            s.mcsStart.s = s.subsections[0].mcsStart.s
            s.mcsStart.l = s.subsections[0].mcsStart.l
            # end is at last aubsection of section
            s.mcsEnd.x = s.subsections[-1].mcsEnd.x
            s.mcsEnd.y = s.subsections[-1].mcsEnd.y
            s.mcsEnd.z = s.subsections[-1].mcsEnd.z
            s.mcsEnd.s = s.subsections[-1].mcsEnd.s
            s.mcsEnd.l = s.subsections[-1].mcsEnd.l
            # middle is the middle point between end and start
            s.mcsMid.x = (s.mcsEnd.x - s.mcsStart.x) / 2.0 + s.mcsStart.x
            s.mcsMid.y = (s.mcsEnd.y - s.mcsStart.y) / 2.0 + s.mcsStart.y
            s.mcsMid.z = (s.mcsEnd.z - s.mcsStart.z) / 2.0 + s.mcsStart.z
            s.mcsMid.s = (s.mcsEnd.s - s.mcsStart.s) / 2.0 + s.mcsStart.s
            s.mcsMid.l = (s.mcsEnd.l - s.mcsStart.l) / 2.0
            # length of the section
            s.l = s.mcsEnd.l - s.mcsStart.l

    def calculateTCS(self):
        offset = 0
        if self.kind == 'TARGET':
            offset = 100.0
        elif self.kind == 'DUMP':
            offset = 59300.0

        # this is last element of A2T, 100mm from the TCS
        tcsEl = self.elements[-1]
        # calculate TCS for elements
        for el in self.elements:
            if not el.isMarker():
                el.tcsStart.x = el.mcsStart.x
                el.tcsStart.y = el.mcsStart.y - tcsEl.mcsStart.y
                el.tcsStart.z = el.mcsStart.z - tcsEl.mcsStart.z - offset
                el.tcsStart.l = el.mcsStart.l
                el.tcsStart.s = el.mcsStart.s

                el.tcsMid.x = el.mcsMid.x
                el.tcsMid.y = el.mcsMid.y - tcsEl.mcsMid.y
                el.tcsMid.z = el.mcsMid.z - tcsEl.mcsMid.z - offset
                el.tcsMid.l = el.mcsMid.l
                el.tcsMid.s = el.mcsMid.s

                el.tcsEnd.x = el.mcsEnd.x
                el.tcsEnd.y = el.mcsEnd.y - tcsEl.mcsEnd.y
                el.tcsEnd.z = el.mcsEnd.z - tcsEl.mcsEnd.z - offset
                el.tcsEnd.l = el.mcsEnd.l
                el.tcsEnd.s = el.mcsEnd.s

        # calculate TCS for sections and subsections
        for s in self.sections:
            # first calculate subsections positions
            for ss in s.subsections:
                ss.tcsStart.x = ss.mcsStart.x
                ss.tcsStart.y = ss.mcsStart.y - tcsEl.mcsStart.y
                ss.tcsStart.z = ss.mcsStart.z - tcsEl.mcsStart.z - offset
                ss.tcsStart.s = ss.mcsStart.s
                ss.tcsStart.l = ss.mcsStart.l

                ss.tcsMid.x = ss.mcsMid.x
                ss.tcsMid.y = ss.mcsMid.y - tcsEl.mcsMid.y
                ss.tcsMid.z = ss.mcsMid.z - tcsEl.mcsMid.z - offset
                ss.tcsMid.s = ss.mcsMid.s
                ss.tcsMid.l = ss.mcsMid.l

                ss.tcsEnd.x = ss.mcsEnd.x
                ss.tcsEnd.y = ss.mcsEnd.y - tcsEl.mcsEnd.y
                ss.tcsEnd.z = ss.mcsEnd.z - tcsEl.mcsEnd.z - offset
                ss.tcsEnd.s = ss.mcsEnd.s
                ss.tcsEnd.l = ss.mcsEnd.l

            # second calculate the section position using subsection position calculated above
            s.tcsStart.x = s.mcsStart.x
            s.tcsStart.y = s.mcsStart.y - tcsEl.mcsStart.y
            s.tcsStart.z = s.mcsStart.z - tcsEl.mcsStart.z - offset
            s.tcsStart.s = s.mcsStart.s
            s.tcsStart.l = s.mcsStart.l

            s.tcsMid.x = s.mcsMid.x
            s.tcsMid.y = s.mcsMid.y - tcsEl.mcsMid.y
            s.tcsMid.z = s.mcsMid.z - tcsEl.mcsMid.z - offset
            s.tcsMid.s = s.mcsMid.s
            s.tcsMid.l = s.mcsMid.l

            s.tcsEnd.x = s.mcsEnd.x
            s.tcsEnd.y = s.mcsEnd.y - tcsEl.mcsEnd.y
            s.tcsEnd.z = s.mcsEnd.z - tcsEl.mcsEnd.z - offset
            s.tcsEnd.s = s.mcsEnd.s
            s.tcsEnd.l = s.mcsEnd.l

    def handleMarker(self, element):
        slotIndex = 1
        slotIndexInc = True

        if re.match('ISRC_LEBT_Optical', element.name):
            self.section = 'LEBT'
            self.slot = ''
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('LEBT_RFQ_Optical', element.name):
            self.section = 'RFQ'
            self.slot = ''
            self.slotIndexes = dict()
            self.elementIndexMult = 1
        elif re.match('RFQ_MEBT_Optical', element.name):
            self.section = 'MEBT'
            self.slot = ''
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('MEBT_DTL1_Optical', element.name):
            self.section = 'DTL'
            self.slot = ''
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('DTL_\d/\d_\d', element.name):
            self.slot = ''
        elif re.match('DTL_\d_\d/\d', element.name):
            self.slot = ''
            slotIndexInc = False
        elif re.match('DTL5_Spoke_Optical', element.name):
            self.section = 'Spk'
            self.slot = 'LDP'
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('CRYO_SPK_in_\d\d', element.name):
            self.slot = 'Crm'
            slotIndexInc = False
        elif re.match('CRYO_SPK_out_\d\d', element.name):
            self.slot = 'LWU'
            slotIndex = 2
        elif re.match('SPK_MBL_Optical', element.name):
            self.section = 'MBL'
            self.slot = 'LWU'
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('CRYO_MBL_in_\d\d', element.name):
            self.slot = 'Crm'
            slotIndexInc = False
        elif re.match('CRYO_MBL_out_\d\d', element.name):
            self.slot = 'LWU'
        elif re.match('MBL_HBL_Optical', element.name):
            self.section = 'HBL'
            self.slot = 'LWU'
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('CRYO_HBL_in_\d\d', element.name):
            self.slot = 'Crm'
            slotIndexInc = False
        elif re.match('CRYO_HBL_out_\d\d', element.name):
            self.slot = 'LWU'
        elif re.match('HBL_HEBT_Optical', element.name):
            self.section = 'HEBT'
            self.slot = 'LWU'
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('DRIFT_HEBT_in_\d\d', element.name):
            self.slot = 'Drf'
            slotIndexInc = False
        elif re.match('DRIFT_HEBT_out_\d\d', element.name):
            self.slot = 'LWU'
        elif re.match('Dipole_In_1', element.name):
            self.slot = 'BND'
            slotIndexInc = False
        elif re.match('Dipole_Out_1', element.name):
            slotIndexInc = False
        elif re.match('HEBT_A2T_Optical', element.name):
            self.section = 'A2T'
            self.slot = 'XXX'
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        elif re.match('Dog_LWU_in_\d\d', element.name):
            self.slot = 'LWU'
        elif re.match('Dog_LWU_out_\d\d', element.name):
            self.slot = 'Drf'
        elif re.match('Dipole_In_2', element.name):
            self.slot = 'BND'
        elif re.match('Dipole_Out_2', element.name):
            pass
        elif re.match('A2T_Horizontal_Start', element.name):
            self.slot = 'Drf'
        elif re.match('A2T_Steerer_In_\d\d', element.name):
            self.slot = 'STR'
        elif re.match('A2T_Steerer_Out_\d\d', element.name):
            self.slot = 'Drf'
        elif re.match('A2T_LWU_In_\d\d', element.name):
            self.slot = 'LWU'
        elif re.match('A2T_LWU_Out_\d\d', element.name):
            self.slot = 'Drf'
        elif re.match('A2T_Raster_In_\d\d', element.name):
            self.slot = 'RST'
        elif re.match('A2T_Raster_Out_\d\d', element.name):
            self.slot = 'Drf'
        elif re.match('DRIFT_DMPL_In_\d\d', element.name):
            self.section = 'DMPL'
            self.slot = 'XXX'
            self.slotIndexes = dict()
            self.elementIndexMult = 10
        else:
            return False

        if slotIndexInc:
            if self.slot in self.slotIndexes:
                self.slotIndexes[self.slot] += 1
            else:
                self.slotIndexes[self.slot] = slotIndex
            # cell value depends on the section
            if self.section == 'LEBT' or \
                self.section == 'RFQ' or \
                self.section == 'MEBT' or \
                self.section == 'DTL':
                self.cell = '%02d' % (self.slotIndexes[self.slot])
            else:
                self.cell = '%03d' % (self.slotIndexes[self.slot] * 10)
            self.elementIndexes = dict()

        s = self.getSection(self.section)
        if not s:
            s = Section(self.section)
            self.sections.append(s)

        self.subsection = self.cell
        if len(self.slot):
            self.subsection += '_' + self.slot
        ss = s.getSubsection(self.subsection)
        if not ss:
            s.subsections.append(Subsection(self.section, self.cell, self.slot))

        return True

    def outputElements(self, position):
        #print('********************** ELEMENTS %s GEOMETRY **********************' % position)

        file = self.folder + '/elements_' + position.lower() + '.csv'
        filehandle = open(file, 'w+')
        hdr = '%7s, %7s, %7s, %7s, %7s, %7s, %10s, %10s, %10s, %10s, %10s, %10s, %10s, %10s, %10s' % \
            ('"sect"', '"cell"', '"slot"', '"elem"', '"index"', '"disc"',
             '"len[mm]"', '"r[mm]"', '"s[mm]"',
             '"MCSx[mm]"', '"MCSy[mm]"', '"MCSz[mm]"',
             '"TCSx[mm]"', '"TCSy[mm]"', '"TCSz[mm]"')
        #print(hdr)
        filehandle.write(hdr+'\n')
        cnt = 0
        for element in self.elements:
            element.outputElement(filehandle, position)
            cnt += 1
        filehandle.close()
        #print()

        print('Created %s CSV file (saved %d elements)..' % (file, cnt))
        return True

    def outputSubsections(self, position):
        #print('********************** SUBSECTIONS %s GEOMETRY **********************' % position)

        file = self.folder + '/subsections_' + position.lower() + '.csv'
        filehandle = open(file, 'w+')
        hdr = '%7s, %7s, %7s, %10s, %10s, %10s, %10s, %10s, %10s, %10s, %10s' % \
            ('"sect"', '"cell"', '"slot"',
             '"len[mm]"', '"s[mm]"',
             '"MCSx[mm]"', '"MCSy[mm]"', '"MCSz[mm]"',
             '"TCSx[mm]"', '"TCSy[mm]"', '"TCSz[mm]"')
        #print(hdr)
        filehandle.write(hdr+'\n')

        cnt = 0
        for s in self.sections:
            for ss in s.subsections:
                mcs = None
                tcs = None
                if position == 'START':
                    mcs = ss.mcsStart
                    tcs = ss.tcsStart
                elif position == 'MIDDLE':
                    mcs = ss.mcsMid
                    tcs = ss.tcsMid
                elif position == 'END':
                    mcs = ss.mcsEnd
                    tcs = ss.tcsEnd
                else:
                    print('Invalid position %s specified' % position)
                    return False

                d = '%7s, %7s, %7s, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f, %10.3f' % \
                                 ('"%s"' % ss.section,
                                  '"%s"' % ss.cell,
                                  '"%s"' % ss.slot,
                                  ss.l,
                                  mcs.s,
                                  mcs.x,
                                  mcs.y,
                                  mcs.z,
                                  tcs.x,
                                  tcs.y,
                                  tcs.z)
                #print(d)
                filehandle.write(d+'\n')
                cnt += 1
        filehandle.close()
        #print()

        print('Created %s CSV file (saved %d subsections)..' % (file, cnt))
        return True

    def outputSections(self):
        #print('********************** SECTIONS %s GEOMETRY **********************' % position)

        file = self.folder + '/sections.csv'
        filehandle = open(file, 'w+')
        hdr = '%7s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s, %13s' % \
            ('"sect"',
             '"len[mm]"', '"s[mm]"',
             '"MCS,s,x[mm]"', '"MCS,s,y[mm]"', '"MCS,s,z[mm]"',
             '"MCS,m,x[mm]"', '"MCS,m,y[mm]"', '"MCS,m,z[mm]"',
             '"MCS,e,x[mm]"', '"MCS,e,y[mm]"', '"MCS,e,z[mm]"',
             '"TCS,s,x[mm]"', '"TCS,s,y[mm]"', '"TCS,s,z[mm]"',
             '"TCS,m,x[mm]"', '"TCS,m,y[mm]"', '"TCS,m,z[mm]"',
             '"TCS,e,x[mm]"', '"TCS,e,y[mm]"', '"TCS,e,z[mm]"')
        #print(hdr)
        filehandle.write(hdr+'\n')
        cnt = 0
        for s in self.sections:
            d = '{s.fullName:>7}, {s.l:13.3f}, {cs.s:13.3f}'.format(s=s,cs=s.mcsStart)
            for cs in [s.mcsStart,s.mcsMid,s.mcsEnd,s.tcsStart,s.tcsMid,s.tcsEnd]:
                d+=', {cs.x:13.3f}, {cs.y:13.3f}, {cs.z:13.3f}'.format(cs=cs)
            #print(d)
            filehandle.write(d+'\n')
            cnt += 1
        filehandle.close()
        #print()

        print('Created %s CSV file (saved %d sections)..' % (file, cnt))
        return True

def run(args):
    lattice = Lattice(args)
    ret = lattice.run()
    if not ret:
        print('Failed!')
        return 1

    print('Success!')
    return 0

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description="Convert TraceWin lattice to CSV..")
    parser.add_argument('kind', choices=['TARGET', 'DUMP'])
    parser.add_argument('folder')
    parser.add_argument('datfile', help='TraceWin lattice file')
    args = parser.parse_args()
    print(args)

    ret = run(args)
    exit(ret)
