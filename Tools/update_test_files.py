import os, sys, shutil, subprocess, glob

nums=  ['1.0', '2.0', '3.0', '4.0',   '5.0',            '6.0',        '7.0',         '8.0',        '9.0',      '9.1']
names= ['LEBT', 'RFQ', 'MEBT', 'DTL', 'Spoke',  'Medium_Beta',  'High_Beta', 'Contingency', 'Dogleg-A2T', 'DumpLine']
shorts=['LEBT', 'RFQ', 'MEBT', 'DTL', 'Spoke',           'MB',         'HB', 'Contingency',    'Dog-A2T',    'DumpL']

def select_binary():
    import platform
    if platform.system()=='Darwin':
        binary='tracemac64'
    elif platform.system()=='Linux':
        binary='tracelx64'
    fullpath=os.path.join(os.path.expanduser('~'),'TraceWin_exe',binary)
    if not os.path.isfile(fullpath):
        raise ValueError("Cannot find binary, please select 'Exe - Extract Console Executables' in TW menu")
    os.system('ln -s {} .'.format(fullpath))
    return './'+binary


for path in ['part_dtl1.dst','part_prev.dst','*_new.ini']:
    for f in glob.glob(path):
        os.remove(f)

UPDATE_INPUT_DIST=False

for num,name,short in zip(nums, names, shorts):
    if len(sys.argv)>1 and short not in sys.argv:
        for path in ['part_dtl1.dst','part_prev.dst','*_new.ini']:
            for f in glob.glob(path):
                os.remove(f)
        continue
    if short=='DumpL':
        # to make sure we do not make a mess with input file
        for path in ['part_dtl1.dst','part_prev.dst','*_new.ini']:
            for f in glob.glob(path):
                os.remove(f)
    print "Name:",name
    if name=='DumpLine':
        projFile='Dogleg-A2T.ini'
    else:
        projFile='{}.ini'.format(name)

    if UPDATE_INPUT_DIST or short in ['RFQ']:
        cmd=[select_binary(), projFile, 'hide_esc', 'nbr_part1=100000']
        if os.path.isfile('part_prev.dst'):
            cmd.append('dst_file1=part_prev.dst')
    else:
        cmd=[select_binary(), projFile, 'hide_esc', 'nbr_part1=100', 'current1=0']

    shutil.copy('ProjectFiles/{}'.format(projFile), projFile)
    os.system('python Tools/merger.py -o {}'.format(short))
    print(' '.join(cmd))
    retval=subprocess.call(cmd)
    if retval!=1:
        raise ValueError("TraceWin returned error {}".format(retval))
    if not UPDATE_INPUT_DIST and short not in ['RFQ']:
        print("Ignore the TraceWin particle loss, we only care about envelope mode")

    if UPDATE_INPUT_DIST:
        shutil.copy('part_dtl1.dst', 'part_prev.dst')
        shutil.move('part_dtl1.dst', 'part_{}.dst'.format(short))
    if name=='LEBT':
        folder='1.0_IonSource_LEBT'
    else:
        folder='{}_{}'.format(num,name)
    if short in ['LEBT', 'MEBT', 'DTL', 'Spoke', 'MB', 'HB', 'Contingency', 'Dog-A2T', 'DumpL']:
        shutil.copy('tracewin.out', '{}/Beam_Physics/OutputFiles/tracewin.out'.format(folder))
    else:
        shutil.copy('partran1.out', '{}/Beam_Physics/OutputFiles/partran1.out'.format(folder))
    if name!='DumpLine' and UPDATE_INPUT_DIST:
        shutil.copy('{}_new.ini'.format(name), 'ProjectFiles/{}'.format(projFile))

