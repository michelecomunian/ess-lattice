#!/bin/sh

# An example hook script to verify what is about to be pushed.  Called by "git
# push" after it has checked the remote status, but before anything has been
# pushed.  If this script exits with a non-zero status nothing will be pushed.
#
# This hook is called with the following parameters:
#
# $1 -- Name of the remote to which the push is being done
# $2 -- URL to which the push is being done
#
# If pushing without using a named remote those arguments will be equal.
#
# Information about the commits which are being pushed is supplied as lines to
# the standard input in the form:
#
#   <local ref> <local sha1> <remote ref> <remote sha1>
#
# This sample shows how to prevent push of commits where the log message starts
# with "WIP" (work in progress).

remote="$1"
url="$2"

check_section() {

    if [ `uname -s` = "Darwin" ]
    then
        [ -f tracemac64 ] || ln -s ~/TraceWin_exe/tracemac64 tracemac64
        TRACEWIN=tracemac64
    elif [ `uname -s` = "Linux" ]
    then
        [ -f tracelx64 ] || ln -s ~/TraceWin_exe/tracelx64 tracelx64
        TRACEWIN=tracelx64
    else
        "WARNING: Cannot check $2 on your platform"
        exit 0
    fi

    echo "Checking $2"

    cp ProjectFiles/$1.ini Project.ini
    python Tools/merger.py -o $2 > /dev/null
    ./$TRACEWIN Project.ini hide_esc nbr_part1=100 current1=0 > /dev/null
    python Tools/check_output.py $3 > /dev/null
    e=$?
    if [ "$e" != "0" ]
    then
        echo "ERROR: Checks in LEBT failed"
        exit $e
    fi
    python Tools/merger.py > /dev/null

    rm Project.ini
    rm Project_new.ini
    rm $TRACEWIN
}


while read local_ref local_sha remote_ref remote_sha
do
    if [ "$remote_ref" = "refs/heads/next" ]
    then
        # To make sure we get back in old state afterwards..
        git diff --quiet
        if [ $? -ne 0 ]
        then
            git stash > /dev/null
            stashed=1
        else
            stashed=0
        fi
        current_state=`git rev-parse --abbrev-ref HEAD`
        git checkout $local_sha > /dev/null 2>&1

        # The list of changes..
        changes=`git diff --name-status -r $remote_sha..$local_sha`
        if [[ $changes =~ .*"1.0_IonSource_LEBT/Beam_Physics".* ]]
        then
            check_section LEBT LEBT 1.0_IonSource_LEBT $local_sha || exit $?
        fi
        if [[ $changes =~ .*"3.0_MEBT/Beam_Physics".* ]]
        then
            check_section MEBT MEBT 3.0_MEBT $local_sha
        fi
        if [[ $changes =~ .*"4.0_DTL/Beam_Physics".* ]]
        then
            check_section DTL DTL 4.0_DTL $local_sha
        fi
        if [[ $changes =~ .*"5.0_Spoke/Beam_Physics".* ]]
        then
            check_section Spoke Spoke 5.0_Spoke $local_sha
        fi
        if [[ $changes =~ .*"6.0_Medium_Beta/Beam_Physics".* ]]
        then
            check_section Medium_Beta MB 6.0_Medium_Beta $local_sha
        fi
        if [[ $changes =~ .*"7.0_High_Beta/Beam_Physics".* ]]
        then
            check_section High_Beta HB 7.0_High_Beta $local_sha
        fi

        # get back to current state
        git reset --hard > /dev/null
        git checkout $current_state > /dev/null 2>&1
        if [ $stashed -ne 0 ]
        then
            git stash pop > /dev/null
        fi
    fi
done

exit 0
