python Tools/tracewin2csv.py TARGET . lattice.dat

for f in CSV_Tables/*
do
    file=$(basename "$f")
    echo "Checking $file"
    diff $file $f || exit $?
done

rm elements_*.csv sections.csv subsections_*.csv
