This repository contains the baseline ESS lattices from Beam Physics.
=====================================================================

[![build status](https://gitlab01.esss.lu.se/ci/projects/4/status.png?ref=next)](https://gitlab01.esss.lu.se/ess-bp/ess-lattice/commits/next)

Which version should I use?
--------------------------

Rule no. 1: Ask a member of the beam physics work package.

The master branch holds the latest approved lattice. Each approved lattice is tagged, so when you take a version remember which tag you used. The master branch is under change control from now on, as decided in the [18th TB](https://indico.esss.lu.se/event/499/).

The next branch holds changes by beam physics which are expected to be correct and ready. However, rigorous testing and checks have *not* been performed on this branch.

If you want to copy/clone from git, you should get your files from the [bitbucket repository](https://bitbucket.org/europeanspallationsource/ess-lattice) which is the official storage. The two aforementioned branches are kept in sync with the gitlab repository used internally by beam physics. For use of any other branches found on the gitlab repository (or any other clone), use at your own risk. A good practice is to check who has put recent developments on the given branch and ask that person if it makes sense to select that version for your intended use.

## Instrumentation numbering system

We use 5-digit numbers XYZnn to index an instrumentation. X identifies the section, Y physics, Z family and the last two as index.

  * X: Section
    1. LEBT
    2. MEBT
    3. DTL
    4. SPK
    5. MB
    6. HB
    7. UHB
    8. A2T
    9. Dumpline
  * Y: Physics
    * 0 Twiss
    * 1 Beam size
    * 2 Special linear (for commands used in HEBT, diag_waist, diag_set_matrix, diag_set_achromat, ...)
    * 3 Special multi-particle (multi-particle optimization such as emittance, transmission, loss, and etc, in case we do.)
    * 4-8 Reserved
    * 9 Centroid
  * Z: Family
   * An extra index to distinguish cases with the same X and Y. (To distinguish planes, global/local, and etc)
   * Start from 0.


## Information for developers

Please add the git hooks from Tools/git-hooks to your repository clone:

```
ln -s ../../Tools/git-hooks/pre-push .git/hooks/pre-push
```

## Information about the lattice versions

### 2016.v2

For all segments, diagnostics have been added and are for the most part now up to date. Diagnostic and actuator ID numbers are updated according to new specs. Markers are added which defines optical and mechanical interfaces between sections.
No error study was performed on this version (the beam optics is not matched).

- LEBT
 - Updated LEBT according to latest drawings from INFN
 - Adjusted length of cone (240mm -> 210mm)
- MEBT
 - Number of BPMs increased to 7
 - Steerer max value and b3 updated
 - Geometry of chopper and its dump updated 
- DTL
 - Now using absolute phases instead of relative.
- Spokes
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - New LWU apertures (57.1mm)
 - Steerers moved to match drawings
 - New solenoid field maps
- Medium Beta
 - Drifts have changed (shorter LWUs)
 - Field maps updated (aperture for cavities)
 - Steerer max/min updated
 - Steerers moved to match drawings
- High Beta
 - Drifts have changed (shorter LWUs)
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - Steerers moved to match drawings
- Contingency
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - Steerers moved to match drawings
- A2T
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - Steerers moved to match drawings
 - Introduced displaced apertures in monolith
 - Changed distances up to and including NSW
 - Adjusted monolith drifts
 - Updated BPMs according to PBI report


### FDSL_2012_05_09

The May release was the 2012 baseline. Before that the lattice was changing more rapidly to keep one as baseline.
******
2012.10 from ESS_Linac/Misc/2012.10.zip
******
Optimus
25 July 2013, Mamad

The Field for the Spokes is from Guillaume Olry
The MB comes from Gabriele Costanza
The HB field comes from Guillaume Devanz.

The E_acc are, 9, 16.785, 19.943 MV/m respectively.
******

### RFQ2TGT
 - Optional Ver (?) of MEBT 2013 v4 (2013 Oct 15th)
 - Matched to the RFQ of Aug 2013.
 - Matched to DTL v84.
 - 3rd buncher moved to reduce E0TL.
***************
MEBT 2014.v1
 - This is MEBT 2014.v1 but also includes the following sections:

   * DTL : v86
   * SC  : Optimus+ (with field maps Spoke/MB/HB_F2F.edz)
   * HEBT: raster27

 - The input is from RFQ of 2013.10.
 - Matching in z is done with a python script.
 - The error are standard tolerances presented in TAC 2014.06.

 - An error study for the MEBT col in Oct 2014 is done with a wrong set of
   dynamic errors for the MEBT buncher cavities. See below. 
***************
2014_Baseline/MEBT_TGT(R29)

MEBT: lat.mebt_2014.v1_nom
DTL: DTL_v86_presented_at_LINAC14
SCL: OptimusPlus_2014BL_ModSpkPhase
HEBT: HEBT_raster29_TGT with ramped phase advance in the contingency

3D Fieldmaps

