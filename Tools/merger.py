from __future__ import print_function, division

import os, sys

FOLDERS={
        "LEBT":   "1.0_IonSource_LEBT",
        "RFQ":   "2.0_RFQ",
        "MEBT":  "3.0_MEBT",
        "DTL":   "4.0_DTL",
        "Spoke": "5.0_Spoke",
        "MB":    "6.0_Medium_Beta",
        "HB":    "7.0_High_Beta",
        "Contingency": "8.0_Contingency",
        "Dog-A2T": "9.0_Dogleg-A2T",
        "DumpL": "9.1_DumpLine",
        }

# Ordered list of all segments:
ALL_SEGMENTS=["LEBT", "RFQ", "MEBT", "DTL", "Spoke", "MB", "HB", "Contingency", "Dog-A2T", "DumpL"]
DEFAULT_SEGMENTS=["LEBT", "RFQ", "MEBT", "DTL", "Spoke", "MB", "HB", "Contingency", "Dog-A2T"]

ADDITIONAL_FILES={
        "RFQ": ["ESS_RFQ_v2.vane"],
        }

# We need to be strict about the number of dashes that marks our info lines
# so that we can find them when inverting the merge action:
NUM_DASHES=50

def get_beam_file():
    import shutil

    if DEFAULT_SEGMENTS[0]=="MEBT":
        shutil.copyfile("Beam_Distributions/RFQ_Beams/dist.rfq_rfq.2013.10_4sig.1M.dst", "dist.rfq_rfq.2013.10_4sig.1M.dst")
    elif DEFAULT_SEGMENTS[0]=="LEBT":
        shutil.copyfile("Beam_Distributions/LEBT ESS 30_06_2014/dst conf1/H1.dst", "H1.dst")
    else:
        print("WARNING, do not know where to find input beam distribution for",DEFAULT_SEGMENTS[0])

def get_other_files():
    import shutil

    for segment in DEFAULT_SEGMENTS:
        if segment in ADDITIONAL_FILES:
            for _file in ADDITIONAL_FILES[segment]:
                shutil.copyfile("{}/Beam_Physics/{}".format(FOLDERS[segment],_file), _file)

def define_and_handle_args():
    import argparse

    global DEFAULT_SEGMENTS

    parser = argparse.ArgumentParser(description="Merges segment lattices into one large..")

    parser.add_argument("-a", dest="adds", nargs="+", default=[],
            help="Add segment(-s)")
    parser.add_argument("-r", dest="removes", nargs="+", default=[],
            help="Remove segment(-s)")
    parser.add_argument("-o", dest="only", nargs="+", default=[],
            help="Only include segment(-s)")
    parser.add_argument("-l", dest="print_segment_list", action="store_true",
            help="Print list of available segments and exit")

    parser.add_argument("-d", dest="dimension", choices=[1, 3], type=int, default=1,
            help="Dimension of the field maps")
    parser.add_argument("-b", dest="get_beam_file", action="store_true",
            help="Also get corresponding beam file")
    parser.add_argument("--extra_plot", dest="extra_plot", action="store_true",
            help="Store beam distribution at each interface")
    parser.add_argument("-u", dest="unmerge", action="store_true",
            help="Split major lattice.dat back instead (reverse action)")

    args = parser.parse_args()

    if args.print_segment_list:
        # sort dictionary by values:
        import operator
        # First we create a dictionary where values are the folder number (flaots):
        my_dict={}
        for k in FOLDERS:
            my_dict[k]=float(FOLDERS[k].split('_')[0])
        # Then we sort using the values (second item in each tuple):
        items=sorted(my_dict.items(), key=operator.itemgetter(1))
        # Finally we create the list of keys (strings):
        keys=[i[0] for i in items]
        print(' '.join(keys))
        sys.exit(0)

    if args.only!=[]:
        DEFAULT_SEGMENTS=args.only

    for segment in args.adds:
        if segment not in DEFAULT_SEGMENTS:
            DEFAULT_SEGMENTS.append(segment)
        else:
            print("You asked to add",segment,", but it is already in list")

    for segment in args.removes:
        if segment in DEFAULT_SEGMENTS:
            del DEFAULT_SEGMENTS[DEFAULT_SEGMENTS.index(segment)]
        else:
            print("You asked to remove",segment,", but it is not in list")

    check_segmentlist_sanity()

    if args.get_beam_file:
        get_beam_file()

    get_other_files()

    return args


def check_segmentlist_sanity():
    '''
    Trying to disallow the user to select lines that do not work
    (for example Spoke+HB without MB)
    '''

    global DEFAULT_SEGMENTS

    # make sure the segments are listed in order:
    FOLDER_NUM={}
    for segment in FOLDERS:
        FOLDER_NUM[segment]=float(FOLDERS[segment].split('_')[0])
    DEFAULT_SEGMENTS=sorted(DEFAULT_SEGMENTS, key=lambda segment: FOLDER_NUM[segment])

    # Construct the allowed lines:
    base=["LEBT", "RFQ", "MEBT", "DTL", "Spoke", "MB", "HB"]
    allowed_lines=[base+["Contingency", "Dog-A2T"],base+["Contingency","DumpL"]]

    validated=False
    for line in allowed_lines:
        if DEFAULT_SEGMENTS[0] in line:
            i0=line.index(DEFAULT_SEGMENTS[0])
            i1=i0+len(DEFAULT_SEGMENTS)
            if len(line)>=i1:
                if line[i0:i1]==DEFAULT_SEGMENTS:
                    validated=True
                    break
    
    if not validated:
        raise ValueError("Your requested list of segments is not valid")


def get_file_list(segments):
    '''
    Returns a list of the lattice files
    '''
    ret=[]
    for s in segments:
        ret.append( os.path.join(FOLDERS[s],'Beam_Physics','lattice.dat') )
    return ret

def write_delimiter(lines, n1, n2, extra_plot):
    '''
    Writes delimiter between sections in the lattice file
    '''
    # First we remove any empty lines end of current file:
    while len(lines[-1].strip())==0:
        del lines[-1]

    txt='; -- END '+n1+', START '+n2
    txt+=' '*(NUM_DASHES-len(txt)-1)+' --\n'
    dashes='-'*NUM_DASHES
    lines.append('\n; '+dashes+'\n')
    lines.append(txt)
    lines.append('; '+dashes+'\n\n')
    if extra_plot:
        lines.append('PLOT_DST'+'\n')

def disable_end(line):
    '''
    Comments out any "end" commands
    '''
    if line.strip().lower()=='end':
        return ''
    return line

def set_maptype(line,args):
    '''
    Sets the correct maptype in the lattice
    '''
    lsp=line.split()

    if args.dimension==1:
        return line
    if len(lsp)==0:
        return line
    if not lsp[0].lower()=='field_map':
        return line

    if lsp[1]=='100': # RF cavities..
        if args.dimension==3:
            lsp[1]='7700'
        lsp[5]=lsp[6]

    elif lsp[1]=='10': # LEBT solenoids..
        if args.dimension==3:
            lsp[1]='70'

    return ' '.join(lsp)+'\n'

def unset_maptype(line,args):
    '''
    Unset the maptype (for unmerge)

    Default maptype 100 hardcoded
    '''
    lsp=line.split()

    if len(lsp)==0:
        return line
    if not lsp[0].lower()=='field_map':
        return line

    if lsp[1]=='7700':
        lsp[1]='100'
        lsp[5]='0'
    elif lsp[1]=='70':
        lsp[1]='10'

    return ' '.join(lsp)+'\n'


def write_header(lines, segments, args):
    '''
    Write header of the major lattice file
    '''
    lines.reverse() # so we can add to top of file..
    # remember now to write header in reversed order..
    lines.append('FIELD_MAP_PATH Field_Maps/{}D\n\n'.format(args.dimension))
    dashes='-'*NUM_DASHES
    lines.append(';\n; '+dashes+'\n\n')
    lines.append('; Included segments: '+', '.join(segments)+'\n')
    lines.append('; '+dashes+'\n;\n')
    lines.reverse()

def write_major_lattice(args):
    '''
    Writes the large lattice file (combination of the elements)
    '''

    segments=DEFAULT_SEGMENTS
    files=get_file_list(segments)
    output=file('lattice.dat','w')

    print("Adding the segments {}".format(', '.join(segments)))
    lines=[]

    for i in xrange(len(files)):
        is_last= i+1==len(files)
        for line in file(files[i],'r'):
            if not is_last:
                line=disable_end(line)
            line=set_maptype(line,args)
            lines.append(line)
        if not is_last:
            write_delimiter(lines, segments[i], segments[i+1], args.extra_plot)

    write_header(lines, segments, args)

    output.writelines(lines)

def split_major_file(major_file, args):
    '''
    This function finds all sub-parts of the
    lattice file and split into a dictionary

    Returns a list of all segment names,
    and the dictionary of all texts in list form

    WARNING: This will be very fragile if we 
    change anything in the initial merge sequence...
    '''
    # go to start of file:
    major_file.seek(0)

    info_barrier='; '+'-'*NUM_DASHES

    is_info=False
    is_first_after=False

    # will hold a dictionary of all segments,
    # for each segment will contain the text to be
    # written to the file of that lattice file
    segment_text={}

    # While this is none we ignore anything written to the file (before our initial header)
    # e.g. TraceWin adds an automatic header in recent versions..
    segment=None

    # the segment we are currently writing to
    current_segment=0

    for l in major_file:
        if l.strip()==info_barrier: # at a start/end of info text
            if is_info:
                is_info=False
                is_first_after=True
            else:
                is_info=True
        elif is_info:
            info=l.split() # remove '; '
            if len(info)==1: # empty line
                continue
            elif info[1]=='Included': # top header
                segments=[i.strip(',') for i in info[3:]]
                segment=segments[0]
            elif info[1]=='--': # segment change
                if info[3].strip(',')==segments[current_segment] and info[5]==segments[current_segment+1]:
                    current_segment+=1
                    segment=segments[current_segment]
                else:
                    raise ValueError("Something went wrong, not the segments we expected to see now in the file!")
            else:
                raise ValueError("Something went wrong")
        
        elif not is_info:
            l=unset_maptype(l,args)
            if is_first_after:
                is_first_after=False
            elif 'FIELD_MAP_PATH' in l:
                is_first_after=True
            elif segment!=None:
                if segment in segment_text:
                    segment_text[segment].append(l)
                else:
                    segment_text[segment]=[l]

    return segments,segment_text

def text_append_end(textlist):
    textlist.append('\nEND\n\n')

def split_major_lattice(args):
    '''
    Splits the major file into the separate pieces again

    Ignores the list of segments.
    Do perhaps need an "only" argument in the future?

    '''

    major_file=file('lattice.dat','r')
    segments,segment_text=split_major_file(major_file, args)

    output_file_list=get_file_list(segments)

    # Write all separate files...
    for segment,fname in zip(segments,output_file_list):
        print("Writing",segment,"into",fname)
        text=segment_text[segment]
        if not fname==output_file_list[-1]:
            text_append_end(text)
        file(fname,'w').write(''.join(text))



def main():
    args=define_and_handle_args()

    if args.unmerge:
        split_major_lattice(args)
    else:
        write_major_lattice(args)

if __name__=="__main__":
    main()
